<!DOCTYPE html>
<html lang="fr">
<head>
	<title>D&D helper</title>
	<script src="https://unpkg.com/vue"></script>
	<style>
		* {
			box-sizing: border-box;
		}
		html, body, #app {
			height: 100%;
			margin: 0;
		}
		#app {
			display: flex;
		}

		.characters {
			border-collapse: collapse;
			border: 1px solid black;
		}

		.characters th, .characters td {
			padding: 2px 5px;
			border-bottom: 1px solid black;
		}

		.pv-bar {
			display: inline-block;
			width: 3px;
			height: 18px;
			position: relative;
			top: 2px;
			margin-right: 1px;
			background-color: currentColor;
		}

		.col-seprator {
			border-right: 1px solid black;
		}
	</style>
</head>
<body>
<div id="app">
	<div style="display: flex; flex: 1; flex-direction: column; padding: 8px">
		<div style="margin-bottom: 8px">
			<button @click="addPc">Add PC</button>
			<select v-model="selectedNpc">
				<option :value="null"></option>
				<option v-for="npcModel in npcModels" :value="npcModel">
					{{ npcModel.name }}
				</option>
			</select>
			<button @click="addNpc" :disabled="selectedNpc === null">Add NPC</button>
			<button @click="nextTurn">Turn</button>
			-
			<button @click="exportState">Export</button>
			<input type="file" accept=".json" @change="importState"/>
		</div>
		<div style="flex: 1">
			<table class="characters">
				<thead>
				<tr>
					<th></th>
					<th style="text-align: right" class="col-seprator">Init</th>
					<th style="text-align: right">Nom</th>
					<th style="text-align: left" class="col-seprator">#</th>
					<th style="text-align: right" class="col-seprator">CA</th>
					<th colspan="5" class="col-seprator">PV</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				<tr v-for="character in characterList" class="character">
					<td @click="setTurn(character)">
						<template v-if="character.current">
							&rightarrow;
						</template>
					</td>
					<td @click="editInit(character)" style="text-align: right" class="col-seprator">{{ character.init }}</td>
					<td @click="editName(character)" style="text-align: right">{{ character.name }}</td>
					<td @click="editCounter(character)" class="col-seprator">{{ character.counter }}</td>
					<td @click="editCA(character)" style="text-align: right" class="col-seprator">{{ character.ca }}</td>

					<td @click="editPVCurrent(character)" style="text-align: right">{{ character.pv.current }}</td>
					<td>/</td>
					<td @click="editPVMax(character)">{{ character.pv.max }}</td>
					<td v-if="character.pv.current > 0" @click="editPVCurrent(character)" :style="{color: character.pv.current/character.pv.max > 0.5 ? 'black' : 'red'}">
						<div v-for="x in Math.min(10, Math.round(10*character.pv.current/character.pv.max))" class="pv-bar"></div>
					</td>
					<td v-else style="font-family: monospace; text-align: center" @click="editPVCurrent(character)">
						DEAD
					</td>
					<td @click="editPVTemp(character)" class="col-seprator">+{{ character.pv.temp }}</td>

					<td>
						<button @click="remove(character)">X</button>
						<button v-if="character.modelId" @click="selectNpc(character.modelId)">Select</button>
					</td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
	<iframe style="width: 490px; border: 0" :src="detailUrl"></iframe>
</div>

<script>
	const npcs = <?= file_get_contents('../npcs.json'); ?>;
	let data;

	try
	{
		data = JSON.parse(localStorage.getItem('data'));
	}
	catch (e)
	{
		data = null;
	}

	if(data === null)
	{
		data = {
			nextId: 1,
			selectedNpc: null,
			pc: [],
			npc: [],
		};
	}

	data.npcModels = <?= file_get_contents('../npcs.json'); ?>;

	var app = new Vue({
		el: '#app',
		data: data,
		computed: {
			characterList()
			{
				return this.pc
					.concat(this.npc)
					.sort((a, b) => b.init - a.init);
			},
			detailUrl()
			{
				if(this.selectedNpc === null)
				{
					return null;
				}

				return 'https://www.aidedd.org/dnd/monstres.php?vf=' + this.selectedNpc.id;
			}
		},
		watch: {
			selectedNpc()
			{
				this.saveState();
			}
		},
		methods: {
			findById(id)
			{
				let c = this.pc.find((c) => c.id === id);

				if( c !== undefined )
				{
					return c;
				}

				return this.npc.find((c) => c.id === id);
			},
			saveState()
			{
				localStorage.setItem(
					'data',
					JSON.stringify(
						Object.assign({}, this.$data, {npcModels: undefined})
					)
				);
			},
			exportState()
			{
				this.saveState();
				const url = window.URL.createObjectURL(new Blob([localStorage.getItem('data')]));
				const a = document.createElement('a');
				a.style.display = 'none';
				a.href = url;
				// the filename you want
				a.download = 'dnd-helper.json';
				document.body.appendChild(a);
				a.click();
				window.URL.revokeObjectURL(url);
				a.remove();
			},
			importState(e)
			{
				const files = e.target.files;

				if( files.length < 1 )
				{
					e.target.value = null;
					return;
				}

				let reader = new FileReader();
				reader.onload = (e) => localStorage.setItem('data', e.target.result);
				reader.readAsText(files[0]);
				location.reload();
			},
			addPc()
			{
				this.pc.push({
					id: this.nextId++,
					current: false,
					counter: '',
					name: '',
					ca: 0,
					init: 0,
					pv: {
						current: 1,
						max: 1,
						temp: 0,
					},
				});

				this.saveState();
			},
			addNpc()
			{
				if( this.selectedNpc === null )
				{
					return;
				}

				const counter = this.npc
					.filter((npc) => npc.modelId === this.selectedNpc.id)
					.length;

				this.npc.push({
					id: this.nextId++,
					current: false,
					modelId: this.selectedNpc.id,
					counter: counter,
					name: this.selectedNpc.name,
					ca: this.selectedNpc.ca,
					init: Math.floor(Math.random() * 20) + 1 + this.selectedNpc.init,
					pv: {
						current: this.selectedNpc.pv,
						max: this.selectedNpc.pv,
						temp: 0,
					},
				});

				this.saveState();
			},
			remove(character)
			{
				if(!window.confirm('Êtes-vous sûr de vouloire supprimé « ' + character.name + '#' + character.counter + ' » ?'))
				{
					return;
				}

				if(this.findById(character.id).current)
				{
					this.nextTurn();
				}

				this.pc = this.pc.filter((c) => c.id !== character.id);
				this.npc = this.npc.filter((c) => c.id !== character.id);
				this.saveState();
			},
			nextTurn()
			{
				const characters = this.characterList
					.concat(this.characterList)
					.filter((c) => c.pv.current > 0);

				if( characters.length === 0 )
				{
					return;
				}

				let wasPrevious = false;

				try
				{
					for (let character of characters)
					{
						if( character.current )
						{
							wasPrevious = true;
							this.findById(character.id).current = false;
						}
						else if(wasPrevious)
						{
							this.findById(character.id).current = true;
							return;
						}
					}

					this.findById(characters[0].id).current = true;
				}
				finally
				{
					this.saveState();
				}
			},
			setTurn(character)
			{
				this.findById(this.characterList.find((c) => c.current).id).current = false;
				this.findById(character.id).current = true;
			},
			edit(object, attribute, message)
			{
				let value = window.prompt(message, object[attribute]);
				
				if(value === null)
				{
					return;
				}

				if(/^[+-][0-9]+$/.test(value))
				{
					if(value[0] === '+')
					{
						value = parseInt(object[attribute]) + parseInt(value.substring(1), 10);
					}
					else if(value[0] === '-')
					{
						value = parseInt(object[attribute]) - parseInt(value.substring(1), 10);
					}
				}
				
				if(Number.isNaN(value))
				{
					return;
				}
				
				object[attribute] = value;

				this.saveState();
			},
			editCounter(character)
			{
				this.edit(character, 'counter', 'Sélectionnez le nouveau compteur de « ' + character.name + '#' + character.counter + ' »');
			},
			editName(character)
			{
				this.edit(character, 'name', 'Sélectionnez le nouveau nom de « ' + character.name + '#' + character.counter + ' »');
			},
			editCA(character)
			{
				this.edit(character, 'ca', 'Sélectionnez la nouvelle CA de « ' + character.name + '#' + character.counter + ' »');
			},
			editInit(character)
			{
				const oldInit = character.init;
				this.edit(character, 'init', 'Sélectionnez la nouvelle initiative de « ' + character.name + '#' + character.counter + ' »');

				character.init = parseFloat(character.init);
				if(Number.isNaN(character.init))
				{
					character.init = oldInit;
				}

				this.saveState();
			},
			editPVCurrent(character)
			{
				this.edit(character.pv, 'current', 'Sélectionnez les pv actuel de « ' + character.name + '#' + character.counter + ' »');
			},
			editPVMax(character)
			{
				this.edit(character.pv, 'max', 'Sélectionnez les pv max de « ' + character.name + '#' + character.counter + ' »');
			},
			editPVTemp(character)
			{
				this.edit(character.pv, 'temp', 'Sélectionnez les pv temporaire de « ' + character.name + '#' + character.counter + ' »');
			},
			selectNpc(id)
			{
				this.selectedNpc = this.npcModels.find((npc) => npc.id === id);
			},
		},
	})
</script>
</body>
</html>
