<?php

function fetchNpcs(): array
{
	$ch = curl_init('https://www.aidedd.org/dnd-tracker/index.php');

	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$body = curl_exec($ch);

	if ($body === false) {
		echo 'Can not fetch monsters';
		exit(1);
	}

	preg_match('@<select id="select_p2".*</select>@', $body, $match);

	if(!preg_match('@<select id="select_p2".*</select>@', $body, $match))
	{
		echo 'Monster list not found';
		exit(1);
	}

	preg_match_all('#<option value="([^;"]+);([^;]+);([^;]+);\+?([^;]+)">([^<]+)#', $match[0], $npcs, PREG_SET_ORDER);

	return array_map(
		function(array $npc)
		{
			return [
				'id' => $npc[1],
				'ca' => (int)$npc[2],
				'pv' => (int)$npc[3],
				'init' => (int)$npc[4],
				'name' => $npc[5],
			];
		},
		$npcs
	);
}

$npcs = fetchNpcs();

file_put_contents('npcs.json', json_encode($npcs));
